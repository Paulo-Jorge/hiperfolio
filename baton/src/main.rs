use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

#[post("/echo")]
async fn echo(req_body: String) -> impl Responder {
    HttpResponse::Ok().body(req_body)
}

async fn manual_hello() -> impl Responder {
    HttpResponse::Ok().body("Hey there!")
}

const PORT:&str = "8001";
//const HOST:&str = "127.0.0.1"; -> for docker should be 0.0.0.0
const HOST:&str = "0.0.0.0";

#[actix_web::main]
async fn main() -> std::io::Result<()> {
	println!(">>> Running on http://{}:{}...", HOST, PORT);
    HttpServer::new(|| {
        App::new()
            .service(hello)
            .service(echo)
            .route("/hey", web::get().to(manual_hello))
    })
    .bind([HOST, PORT].join(":"))?
    .run()
    .await
}

